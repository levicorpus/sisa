#The repository includes:
* Source code of SISA.
* Training code for individual user.
* Pre-trained .clf file for Narendra Modi as user

##SISA.py
It includes code to run object detection, instance segmentation and blurring or encryption on arbitrary images.

##priority_int.py
Includes code to prioritize detected objects.

##decrypt.py
This file contains code to decrypt or deblur modified images. 


##train_user.py
Includes code to prepare .clf file for face recognition.

##Syntax:
cd src
python SISA.py --method_m=<"enc"/ "blur"> --level=<percent change accepted> <path to image/images folder>

python decrypt.py <path to image/images folder>


## Requirements
Python 3.4,
numpy,
scipy,
Pillow,
cython,
matplotlib,
scikit-image,
tensorflow>=1.3.0,
keras>=2.0.8,
opencv-python,
h5py,
imgaug,
exiftool,
opencv,
PIL
