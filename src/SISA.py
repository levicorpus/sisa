import os
import sys
import math
import numpy as np
import priority_int
from PIL import Image
import time
import click
import re
import cv2
from threading import Thread
import pickle 

# Root directory of the project
ROOT_DIR = os.path.abspath("../")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn import utils
import mrcnn.newmodel as modellib

# Import COCO config
sys.path.append(os.path.join(ROOT_DIR, "src/coco/"))  # To find local version
import coco



# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

# Local path to trained weights file
COCO_MODEL_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")
###
# Download COCO trained weights from Releases if needed
#if not os.path.exists(COCO_MODEL_PATH):
   # utils.download_trained_weights(COCO_MODEL_PATH)

# Directory of images to run detection on
#argv1=sys.argv[1]
#IMAGE_DIR = os.path.join(argv1)
# We'll be using a model trained on the MS-COCO dataset. The configurations of this model are in the ```CocoConfig``` class in ```coco.py```.
# For inferencing, modify the configurations a bit to fit the task. To do so, sub-class the ```CocoConfig``` class and override the attributes you need to change.

class InferenceConfig(coco.CocoConfig):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1

config = InferenceConfig()
###config.display()


# ## Create Model and Load Trained Weights

# Create model object in inference mode.
model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config)

# Load weights trained on MS-COCO
model.load_weights(COCO_MODEL_PATH, by_name=True)


# ## Class Names
# The model classifies objects and returns class IDs, which are integer value that identify each class. Some datasets assign integer values to their classes and some don't. For example, in the MS-COCO dataset, the 'person' class is 1 and 'teddy bear' is 88. The IDs are often sequential, but not always. The COCO dataset, for example, has classes associated with class IDs 70 and 72, but not 71.
# To improve consistency, and to support training on data from multiple sources at the same time, our ```Dataset``` class assigns it's own sequential integer IDs to each class. For example, if you load the COCO dataset using our ```Dataset``` class, the 'person' class would get class ID = 1 (just like COCO) and the 'teddy bear' class is 78 (different from COCO). Keep that in mind when mapping class IDs to class names.
# To get the list of class names, you'd load the dataset and then use the ```class_names``` property like this.
# ```
# # Load COCO dataset
# dataset = coco.CocoDataset()
# dataset.load_coco(COCO_DIR, "train")
# dataset.prepare()
# # Print class names
# 
# ```
# We don't want to require you to download the COCO dataset just to run this demo, so we're including the list of class names below. The index of the class name in the list represent its ID (first class is 0, second is 1, third is 2, ...etc.)


# COCO Class names
# Index of the class in the list is its ID. For example, to get ID of
# the teddy bear class, use: class_names.index('teddy bear')
class_names = ['BG', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
               'bus', 'train', 'truck', 'boat', 'traffic light',
               'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
               'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
               'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
               'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
               'kite', 'baseball bat', 'baseball glove', 'skateboard',
               'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
               'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
               'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
               'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
               'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
               'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
               'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
               'teddy bear', 'hair drier', 'toothbrush']




def apply_blur(image,mask):
    """Apply the given mask to the image.

    
    for c in range(2):
        image[:, :, c] = np.where(mask == 1,(image[:,:,c]*np.random.normal(121,101,mask.shape)),image[:, :, c])
    
    """
    for c in range(5):
        image[:, :, c%3] = np.where(mask == 1,cv2.GaussianBlur(image[:,:,c%3],(51,31),501),image[:, :, c%3])
    I= image

def apply_encor(image,boxes,enc_mask):
    

    cnt=2
    #print(boxes)
    for b in boxes:
        
        y1, x1, y2, x2 = b[2]
        
      
      
        for i in range(y1,y2):
            for j in range(x1,x2):
                enc_mask[i][j]=cnt
                cnt+=1

    for c in range(2):
        image[:, :, c] = np.where(enc_mask >0,(image[:,:,c]+enc_mask)%256,image[:, :, c])
    I= image

def apply_enc(image,boxes):
    """Apply the given mask to the image.

    
    for c in range(2):
        image[:, :, c] = np.where(mask == 1,(image[:,:,c]*np.random.normal(121,101,mask.shape)),image[:, :, c])
    
    """
    y1, x1, y2, x2 = boxes[2]
    
    cnt=1
    for i in range(y1,y2):
        for j in range(x1,x2):
            cnt=cnt+1
            l=image.item(i,j,0)
            m=image.item(i,j,1)
            n=image.item(i,j,2)
                
            n= (n+cnt)%256
            m= (m+n)%256
            l= (l-n)%256
            image.itemset((i,j,0),l)
            image.itemset((i,j,1),m)
            image.itemset((i,j,2),n)

    I=image

  
  

def image_files_in_folder(folder):

    return [os.path.join(folder, f) for f in os.listdir(folder) if re.match(r'.*\.(jpg|jpeg|png)', f, flags=re.I)]

@click.command()
@click.argument('image_to_check')
@click.option('--method_m', default="blur", help='Which modification method to use "blur" or "enc"')
@click.option('--level', default=30, help='What percentage of image to modify')

def main(image_to_check, method_m,level):
  
  
  # ## Run Object Detection
  
  # Load images from the images folder
  
  extra=""
  sec_size=0

  if os.path.isdir(image_to_check):
    for image_file in image_files_in_folder(image_to_check):
      
      init_time=time.time()
      img=Image.open(image_file)
      
      info=img.info 
        
      dpi='dpi'
      if dpi in img.info.keys():
        dpi=info['dpi']
      else:
        dpi=(0,0)
      exif='exif'
      if exif in img.info.keys():
        exif=info['exif']
      else:
        exif=bytes(''.encode('utf-8'))

      filename, extension = image_file.split('.')

      if method_m=="enc" and extension!="png":
        extra+=image_file+" "
        
        image_file=filename+".png"
        img.save(image_file,quality=95,dpi=dpi,exif=exif)
        img=Image.open(image_file)

      extra+=image_file+"_original "    
      

      I = np.array(img)
      enc_mask=np.zeros((I.shape[0],I.shape[1]),dtype = int)
      blur_mask=np.zeros((I.shape[0],I.shape[1]),dtype = bool)

      # Run detection
      results,ishape = model.detect([I], verbose=0)
      detect_time=time.time()
      #print('obj detect time '+str(detect_time-init_time))
          
      pr=(priority_int.prioritize(I,results,level))

      # Visualize results
      roislist=list()
      #roislist=res['rois']
      co_or=''

      maskslist=list()
      #maskslist=res['masks']
      yaml_dict=list()

      threads=[] 
      if(method_m=="enc"):
        apply_encor(I,pr,enc_mask)

      else:      
        for i in pr:      
          blur_mask=blur_mask|i[3]

        apply_blur(I,blur_mask)


      im=Image.fromarray(I)      
      
      #print('timee '+str(time.time()-detect_time))

      im.save(image_file,quality=95,dpi=dpi,exif=exif)
      
      #print('timee '+str(time.time()-detect_time))

      sec_size="e"
      diff_method=""

      for l in pr:
        

        co_or+=','+str(l[2][1])+','+str(l[2][0])+','+str(l[2][3])+','+str(l[2][2])

        if method_m=="blur":
          sec_size=str(time.time())+'.pkl'
          
          crimg=img.crop((l[2][1],l[2][0],l[2][3],l[2][2])).copy()            
          yaml_dict.append(crimg) 
          diff_method="b,"

      with open(sec_size, 'wb') as f:
        pickle.dump(yaml_dict, f)

      sec_size=diff_method+sec_size

      os.system('exiftool -config "/home/dell/jarss/Image-ExifTool-11.11/config_files/try.config" -xmp-xmp:coor="'+co_or+'" -xmp-xmp:secsize="'+sec_size+'" "'+image_file+'"')
      
    os.system('rm '+extra)

        
  else:
      init_time=time.time()
      img=Image.open(image_to_check)

      info=img.info 
        
      dpi='dpi'
      if dpi in img.info.keys():
        dpi=info['dpi']
      else:
        dpi=(0,0)
      exif='exif'
      if exif in img.info.keys():
        exif=info['exif']
      else:
        exif=bytes(''.encode('utf-8'))

      filename, extension = image_to_check.split('.')
      if method_m=="enc" and extension!="png":
        
        image_to_check=filename+".png"
        img.save(image_to_check,quality=95,dpi=dpi,exif=exif)
        img=Image.open(image_to_check)

      extra+=image_to_check+"_original "    
      

      I = np.array(img)
      enc_mask=np.zeros((I.shape[0],I.shape[1]),dtype = int)
      # Run detection
      results,ishape = model.detect([I], verbose=0)
      detect_time=time.time()
      #print('obj detect time '+str(detect_time-init_time))
          
      pr=(priority_int.prioritize(I,results,level))

      # Visualize results
      roislist=list()
      #roislist=res['rois']
      co_or=''

      maskslist=list()
      #maskslist=res['masks']
      yaml_dict=list()

      threads=[]    
      
      for i in pr:      
        if(method_m=="enc"):
          t1=Thread(target=apply_encor,args=(I,i,enc_mask))
        else:
          #print(i[3])
          t1=Thread(target=apply_blur,args=(I,i[3]))
        t1.start()
        threads.append(t1)

      for t1 in threads:
        t1.join()

      im=Image.fromarray(I)      
      
      print('timee '+str(time.time()-detect_time))

      im.save(image_to_check,quality=95,dpi=dpi,exif=exif)
      
      print('timee '+str(time.time()-detect_time))

      sec_size="e,"
      diff_method=""

      for l in pr:
        

        co_or+=','+str(l[2][1])+','+str(l[2][0])+','+str(l[2][3])+','+str(l[2][2])

        if method_m=="blur":
          sec_size=str(time.time())+'.pkl'
          
          crimg=img.crop((l[2][1],l[2][0],l[2][3],l[2][2])).copy()            
          yaml_dict.append(crimg) 
          diff_method="b,"

        with open(sec_size, 'wb') as f:
          pickle.dump(yaml_dict, f)

      sec_size=diff_method+sec_size

      os.system('exiftool -config "/home/dell/jarss/Image-ExifTool-11.11/config_files/try.config" -xmp-xmp:coor="'+co_or+'" -xmp-xmp:secsize="'+sec_size+'" "'+image_to_check+'"')
      
      os.system('rm '+extra)
  
        

if __name__ == "__main__":
    main()
