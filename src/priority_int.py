import cv2
import argparse
import numpy as np
import heapq
import math
import math
from sklearn import neighbors
import os
import os.path
import pickle
from PIL import Image, ImageDraw
import face_recognition


ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

def predict(X_img, knn_clf=None, model_path=None, distance_threshold=0.5):
    """
    Recognizes faces in given image using a trained KNN classifier

    :param X_img_path: path to image to be recognized
    :param knn_clf: (optional) a knn classifier object. if not specified, model_save_path must be specified.
    :param model_path: (optional) path to a pickled knn classifier. if not specified, model_save_path must be knn_clf.
    :param distance_threshold: (optional) distance threshold for face classification. the larger it is, the more chance
           of mis-classifying an unknown person as a known one.
    :return: a list of names and face locations for the recognized faces in the image: [(name, bounding box), ...].
        For faces of unrecognized persons, the name 'unknown' will be returned.
    """
    
    if knn_clf is None and model_path is None:
        raise Exception("Must supply knn classifier either thourgh knn_clf or model_path")

    # Load a trained KNN model (if one was passed in)
    if knn_clf is None:
        with open(model_path, 'rb') as f:
            knn_clf = pickle.load(f)

    # Load image file and find face locations
    
    X_face_locations = face_recognition.face_locations(X_img)

    # If no faces are found in the image, return an empty result.
    if len(X_face_locations) == 0:
        return []

    # Find encodings for faces in the test iamge
    faces_encodings = face_recognition.face_encodings(X_img, known_face_locations=X_face_locations)

    # Use the KNN model to find the best matches for the test face
    closest_distances = knn_clf.kneighbors(faces_encodings, n_neighbors=1)
    are_matches = [closest_distances[0][i][0] <= distance_threshold for i in range(len(X_face_locations))]

    # Predict classes and remove classifications that aren't within the threshold
    return [(pred, loc) if rec else ("unknown", loc) for pred, loc, rec in zip(knn_clf.predict(faces_encodings), X_face_locations, are_matches)]
def prioritize(I,results,level):
    #print(ishape)
    wvalue=I.shape[1]
    hvalue=I.shape[0]
    #print(I.shape,wvalue)
    lim_siz=(level*wvalue*hvalue)/100
    cx=wvalue/2
    cy=hvalue/2



    recg=predict(I, model_path="trained_knn_model.clf")
    #print(recg)
    for rec in recg:
        if(rec[0]=='modi'):
            top, right, bottom, left=rec[1]
            
            cx=(right-left)/2 +left
            cy=(bottom-top)/2 +top


    #print(cx,' ',cy)
    r=results[0]
    coor=list()
    mklist=list()
    coor=r['rois']
    mklist=r['masks']
    clsid=list()
    clsid=r['class_ids']
    index=0
    flag=True
    pri=[]
    heapq.heapify(pri)
    dist=0
    cnt=0
   
    for rc,c in zip(coor,clsid):
        #print('rcccc',rc)

        ix=(rc[3]-rc[1])
        iy=(rc[2]-rc[0])
        areaa=ix*iy
        ix=ix/2+rc[1]
        iy=iy/2+rc[0]
        #print(cx,' ',cy)
        #print(ix,' ',iy)
        dist=(((ix-cx)*(ix-cx))+((iy-cy)*(iy-cy)))**0.5
        #print('dist',dist)
        m=mklist[:,:,cnt].copy()
        #print(coor[cnt])
        heapq.heappush(pri,(dist,areaa,rc,m,c))
        cnt=cnt+1
    #heapq.heappush(pri,(label,dist))
    
    
    
    # Traverse through all array elements 
    
    ret=[]
    n = len(pri) 
    #print(pri)
    #res=dict({'masks':[]})
    roiss=[]
    for i in range(n): 
        ##print('priii',pri[i][0])
        ##print('coor',pri[i][2])
        # Last i elements are already in place 
        for j in range(0, n-i-1):
            #print(pri[j][0],pri[j+1][0])

  
            if pri[j][0] < pri[j+1][0] : 
                temp=pri[j]
                pri[j]=pri[j+1]
                pri[j+1]=temp
                #print(pri[j][0],pri[j+1][0])

        
        if lim_siz-pri[n-i-1][1]>0:
            lim_siz=lim_siz-pri[n-i-1][1]
            #print(pri[n-i-1][1])
            flag=False
            ret.append([pri[n-i-1][0],pri[n-i-1][1],pri[n-i-1][2],pri[n-i-1][3],pri[n-i-1][4]])
            
            

        else:
            	break;
    

    """for r in ret:
        print(r[0],r[2])"""
    
    return ret

            	



