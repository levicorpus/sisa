import os
import exiftool
import re
from PIL import Image
import sys
import click
import time
import cv2
import numpy as np
import pickle

def image_files_in_folder(folder):
    return [os.path.join(folder, f) for f in os.listdir(folder) if re.match(r'.*\.(jpg|jpeg|png)', f, flags=re.I)]

@click.command()
@click.argument('image_to_check')
@click.option('--method_m', default="blur", help='Which modification method to use "blur" or "enc"')

def main(image_to_check, method_m):
    
    
    if os.path.isdir(image_to_check):
        for image_file in image_files_in_folder(image_to_check):
            
            with exiftool.ExifTool() as et:
                mdta = et.get_tags(('Secsize','Coor'),image_file)
                #print('mdta',mdta)
                key=mdta['XMP:Secsize']
                svalue=key.split(',') 
                print(svalue)
                key=mdta['XMP:Coor']
                cvalue=re.findall(r'\d+', key)
                method_m=svalue[0]
                if(method_m=="b"):
                    svalue=svalue[1]
                    with open(svalue, 'rb') as f:
                        data = pickle.load(f)

            ori_img=Image.open(image_file)
            I=np.array(ori_img)
            enc_mask=np.zeros((I.shape[0],I.shape[1]),dtype = int)
            #print(len(enc_mask))
                    

            cnt=2
            while cvalue :
            
                    if method_m=="b":
                        sv_img=data[0]                
                        ori_img.paste(sv_img,(int(cvalue[0]),int(cvalue[1]),int(cvalue[2]),int(cvalue[3])))
                        data.pop(0)
                    
                    
                    x1=int(cvalue[0])
                    y1=int(cvalue[1])
                    x2=int(cvalue[2])
                    y2=int(cvalue[3])

                    #print(y1,x1,y2,x2)
                    
                      
                      
                    for i in range(y1,y2):
                        for j in range(x1,x2):

                            enc_mask[i][j]=cnt
                            cnt+=1

                    cvalue.pop(0)
                    cvalue.pop(0)
                    cvalue.pop(0)
                    cvalue.pop(0)

            if method_m=="e":
                for c in range(2):
                    I[:, :, c] = np.where(enc_mask >0,(I[:,:,c]-enc_mask)%256,I[:, :, c])
                    ori_img=Image.fromarray(I)
                ori_img.show()
                   
                
                
                
                    
            info=ori_img.info
            dpi='dpi'
            if dpi in info.keys():
                dpi=info['dpi']
            else:
                dpi=(0,0)
            exif='exif'
            if exif in info.keys():
                exif=info['exif']
            else:
                exif=bytes(''.encode('utf-8'))
            
            ori_img.save(image_file,quality=95,dpi=dpi,exif=exif)

           

    else:
            with exiftool.ExifTool() as et:
                mdta = et.get_tags(('Secsize','Coor'),image_to_check)
                #print('mdta',mdta)
                key=mdta['XMP:Secsize']
                svalue=key.split(',') 
                print(svalue)
                key=mdta['XMP:Coor']
                cvalue=re.findall(r'\d+', key)
                method_m=svalue[0]
                if(method_m=="b"):
                    svalue=svalue[1]
                    with open(svalue, 'rb') as f:
                        data = pickle.load(f)

                    


            while cvalue:
            
                ori_img=Image.open(image_to_check)

                if(method_m=="b"):
                    sv_img=data[0]                
                    ori_img.paste(sv_img,(int(cvalue[0]),int(cvalue[1]),int(cvalue[2]),int(cvalue[3])))
                    data.pop(0)

                else:
                    image = np.array(ori_img)
                    image = image.astype(np.uint8).copy()
                    y1=int(cvalue[0])
                    x1=int(cvalue[1])
                    y2=int(cvalue[2])
                    x2 = int(cvalue[3])
                    cnt=2
                    for i in range(x1,x2):
                            for j in range(y1,y2):
                                
                                
                                    l=image.item(i,j,0)
                                    m=image.item(i,j,1)
                                    n=image.item(i,j,2)
                                    
                                    m= (m-n)%256
                                    l= (l+n)%256
                                    n= (n-cnt)%256
                                    image.itemset((i,j,0),l)
                                    image.itemset((i,j,1),m)
                                    image.itemset((i,j,2),n)

                                    cnt=cnt+1
                    ori_img= Image.fromarray(np.uint8(image))
                
                cvalue.pop(0)
                cvalue.pop(0)
                cvalue.pop(0)
                cvalue.pop(0)
                
                    
                info=ori_img.info
                dpi='dpi'
                if dpi in info.keys():
                    dpi=info['dpi']
                else:
                    dpi=(0,0)
                exif='exif'
                if exif in info.keys():
                    exif=info['exif']
                else:
                    exif=bytes(''.encode('utf-8'))
            
                ori_img.save(image_to_check,quality=95,dpi=dpi,exif=exif)

               

if __name__ == "__main__":
    main()

